//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.7.0
//     from Assets/Controller2D/Input/GameControls.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @GameControls: IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameControls"",
    ""maps"": [
        {
            ""name"": ""GamePlay"",
            ""id"": ""f73663f2-ffe6-461f-847e-709b52833f94"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""c1e6b500-2a4a-4b7e-9edb-d2d3d3677d4a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""9ab540ca-9dc2-421f-8733-33f48339752b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""PickCoin"",
                    ""type"": ""Button"",
                    ""id"": ""63cbae77-e0e9-453a-b690-d3f5c3b773f3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""StoreCoin"",
                    ""type"": ""Button"",
                    ""id"": ""6de26660-81a8-4af4-a69a-d4e32e7343c7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MoveB"",
                    ""type"": ""Value"",
                    ""id"": ""ef3e3f55-4fc8-45c4-8d82-f29a1fb018cf"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""JumpB"",
                    ""type"": ""Button"",
                    ""id"": ""c7ae7374-0d70-449a-ae65-40048ec387cc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""PickCoinB"",
                    ""type"": ""Button"",
                    ""id"": ""f02b1cb8-74df-45df-b149-d85d273b2d34"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""StoreCoinB"",
                    ""type"": ""Button"",
                    ""id"": ""f872a34a-8373-44e6-a0c3-bc4fa5139f9f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""918333a6-fb91-4b4b-896f-a89e207f1159"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d7d37f60-5d5d-45e3-be17-bad7d05d981e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""db642e57-cdd5-4fe5-9be8-abdc56c2efd2"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""2567ecaa-c46d-49ca-a515-83012005d08d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""3b560ba5-e96b-4041-81a9-b9945654aa1b"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""0d94cda9-6ecb-467c-a97f-05cb7e9c8dd4"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d2644bcb-9923-4d6d-a81c-8cd7e59ba7f4"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PickCoin"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e5651c05-f470-4a26-8456-a1770a5787f7"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StoreCoin"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de0824c5-ad28-4328-85ea-ee6d4c8e97c5"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""UDLR"",
                    ""id"": ""8a1e642d-12a2-4058-a53b-d6f679195acf"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveB"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d3f541ad-2d62-4ed9-96fe-b42117f6cf40"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""e07d9c7f-8ec2-40d5-a9b4-14ba5b3e4435"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6da245a1-52f7-4c7f-ab3f-04dc731cc1d0"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""8c1752e2-7bbf-4007-9d55-3313100ca91a"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""70437a7a-98d3-4693-a9f8-1f1e03e3378e"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""JumpB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7bece7db-1865-40a2-be4d-31422778ac0f"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""JumpB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c6fa7d46-25a9-41c8-87ed-338df1b1cd2f"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PickCoinB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""afb43d66-2e32-40f9-b15f-d1f451ae50a9"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PickCoinB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""17a989e1-2180-433b-b6fa-a3d31d174a8a"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StoreCoinB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6f9b5c0b-3c3a-4a2a-bb4d-2d178c15153c"",
                    ""path"": ""<Keyboard>/l"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StoreCoinB"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // GamePlay
        m_GamePlay = asset.FindActionMap("GamePlay", throwIfNotFound: true);
        m_GamePlay_Move = m_GamePlay.FindAction("Move", throwIfNotFound: true);
        m_GamePlay_Jump = m_GamePlay.FindAction("Jump", throwIfNotFound: true);
        m_GamePlay_PickCoin = m_GamePlay.FindAction("PickCoin", throwIfNotFound: true);
        m_GamePlay_StoreCoin = m_GamePlay.FindAction("StoreCoin", throwIfNotFound: true);
        m_GamePlay_MoveB = m_GamePlay.FindAction("MoveB", throwIfNotFound: true);
        m_GamePlay_JumpB = m_GamePlay.FindAction("JumpB", throwIfNotFound: true);
        m_GamePlay_PickCoinB = m_GamePlay.FindAction("PickCoinB", throwIfNotFound: true);
        m_GamePlay_StoreCoinB = m_GamePlay.FindAction("StoreCoinB", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }

    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // GamePlay
    private readonly InputActionMap m_GamePlay;
    private List<IGamePlayActions> m_GamePlayActionsCallbackInterfaces = new List<IGamePlayActions>();
    private readonly InputAction m_GamePlay_Move;
    private readonly InputAction m_GamePlay_Jump;
    private readonly InputAction m_GamePlay_PickCoin;
    private readonly InputAction m_GamePlay_StoreCoin;
    private readonly InputAction m_GamePlay_MoveB;
    private readonly InputAction m_GamePlay_JumpB;
    private readonly InputAction m_GamePlay_PickCoinB;
    private readonly InputAction m_GamePlay_StoreCoinB;
    public struct GamePlayActions
    {
        private @GameControls m_Wrapper;
        public GamePlayActions(@GameControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_GamePlay_Move;
        public InputAction @Jump => m_Wrapper.m_GamePlay_Jump;
        public InputAction @PickCoin => m_Wrapper.m_GamePlay_PickCoin;
        public InputAction @StoreCoin => m_Wrapper.m_GamePlay_StoreCoin;
        public InputAction @MoveB => m_Wrapper.m_GamePlay_MoveB;
        public InputAction @JumpB => m_Wrapper.m_GamePlay_JumpB;
        public InputAction @PickCoinB => m_Wrapper.m_GamePlay_PickCoinB;
        public InputAction @StoreCoinB => m_Wrapper.m_GamePlay_StoreCoinB;
        public InputActionMap Get() { return m_Wrapper.m_GamePlay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GamePlayActions set) { return set.Get(); }
        public void AddCallbacks(IGamePlayActions instance)
        {
            if (instance == null || m_Wrapper.m_GamePlayActionsCallbackInterfaces.Contains(instance)) return;
            m_Wrapper.m_GamePlayActionsCallbackInterfaces.Add(instance);
            @Move.started += instance.OnMove;
            @Move.performed += instance.OnMove;
            @Move.canceled += instance.OnMove;
            @Jump.started += instance.OnJump;
            @Jump.performed += instance.OnJump;
            @Jump.canceled += instance.OnJump;
            @PickCoin.started += instance.OnPickCoin;
            @PickCoin.performed += instance.OnPickCoin;
            @PickCoin.canceled += instance.OnPickCoin;
            @StoreCoin.started += instance.OnStoreCoin;
            @StoreCoin.performed += instance.OnStoreCoin;
            @StoreCoin.canceled += instance.OnStoreCoin;
            @MoveB.started += instance.OnMoveB;
            @MoveB.performed += instance.OnMoveB;
            @MoveB.canceled += instance.OnMoveB;
            @JumpB.started += instance.OnJumpB;
            @JumpB.performed += instance.OnJumpB;
            @JumpB.canceled += instance.OnJumpB;
            @PickCoinB.started += instance.OnPickCoinB;
            @PickCoinB.performed += instance.OnPickCoinB;
            @PickCoinB.canceled += instance.OnPickCoinB;
            @StoreCoinB.started += instance.OnStoreCoinB;
            @StoreCoinB.performed += instance.OnStoreCoinB;
            @StoreCoinB.canceled += instance.OnStoreCoinB;
        }

        private void UnregisterCallbacks(IGamePlayActions instance)
        {
            @Move.started -= instance.OnMove;
            @Move.performed -= instance.OnMove;
            @Move.canceled -= instance.OnMove;
            @Jump.started -= instance.OnJump;
            @Jump.performed -= instance.OnJump;
            @Jump.canceled -= instance.OnJump;
            @PickCoin.started -= instance.OnPickCoin;
            @PickCoin.performed -= instance.OnPickCoin;
            @PickCoin.canceled -= instance.OnPickCoin;
            @StoreCoin.started -= instance.OnStoreCoin;
            @StoreCoin.performed -= instance.OnStoreCoin;
            @StoreCoin.canceled -= instance.OnStoreCoin;
            @MoveB.started -= instance.OnMoveB;
            @MoveB.performed -= instance.OnMoveB;
            @MoveB.canceled -= instance.OnMoveB;
            @JumpB.started -= instance.OnJumpB;
            @JumpB.performed -= instance.OnJumpB;
            @JumpB.canceled -= instance.OnJumpB;
            @PickCoinB.started -= instance.OnPickCoinB;
            @PickCoinB.performed -= instance.OnPickCoinB;
            @PickCoinB.canceled -= instance.OnPickCoinB;
            @StoreCoinB.started -= instance.OnStoreCoinB;
            @StoreCoinB.performed -= instance.OnStoreCoinB;
            @StoreCoinB.canceled -= instance.OnStoreCoinB;
        }

        public void RemoveCallbacks(IGamePlayActions instance)
        {
            if (m_Wrapper.m_GamePlayActionsCallbackInterfaces.Remove(instance))
                UnregisterCallbacks(instance);
        }

        public void SetCallbacks(IGamePlayActions instance)
        {
            foreach (var item in m_Wrapper.m_GamePlayActionsCallbackInterfaces)
                UnregisterCallbacks(item);
            m_Wrapper.m_GamePlayActionsCallbackInterfaces.Clear();
            AddCallbacks(instance);
        }
    }
    public GamePlayActions @GamePlay => new GamePlayActions(this);
    public interface IGamePlayActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnPickCoin(InputAction.CallbackContext context);
        void OnStoreCoin(InputAction.CallbackContext context);
        void OnMoveB(InputAction.CallbackContext context);
        void OnJumpB(InputAction.CallbackContext context);
        void OnPickCoinB(InputAction.CallbackContext context);
        void OnStoreCoinB(InputAction.CallbackContext context);
    }
}
