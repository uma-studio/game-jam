using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Hotran.Control.Dir2
{
    /// <summary>
    /// 横板角色移动
    /// </summary>
    public class PlayerController2Dir : MonoBehaviour
    {
        public enum PlayerTag { A, B }

        [SerializeField]
        private bool actionAvailable = true;

        [SerializeField]
        private PlayerTag playerTag;
        public PlayerTag Tag => playerTag;

        private GameControls controls;

        private Rigidbody2D rgbody;
        private Animator animator;
        private SpriteRenderer spriteRenderer;
        private Collider2D collider2d;

        private CoinPicker coinPicker;
        private CoinThrower coinThrower;
        private CoinContainer coinContainer;

        private float coolTimer;
        [Tooltip("捡起和投掷的间隔时间")]
        [SerializeField]
        private float coolTime = 0.5f;

        private Vector2 moveInput;

        [Tooltip("检测角色是否处于地面的物体")]
        [SerializeField]
        private GroundDetector groundDetector;

        [Tooltip("跳跃尘土粒子效果")]
        [SerializeField]
        private ParticleSystem jumpEffect;

        //[SerializeField]
        //private float moveSpeed = 5f;

        //[SerializeField]
        //private float jumpForce = 10f;

        private bool jumping = false;

        //玩家剩余可跳跃次数
        private int jumpCountLeft;

        private float gravity;

        private void Awake()
        {
            controls = new GameControls();
            rgbody = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            coinPicker = GetComponent<CoinPicker>();
            coinThrower = GetComponent<CoinThrower>();
            coinContainer = GetComponent<CoinContainer>();
            collider2d = GetComponent<Collider2D>();
            coolTimer = coolTime;
        }

        private void OnEnable()
        {
            if (playerTag == PlayerTag.A)
            {
                controls.GamePlay.Move.performed += Move_performed;
                controls.GamePlay.Move.canceled += Move_canceled;
                controls.GamePlay.Jump.started += Jump_started;
                if (actionAvailable)
                {
                    controls.GamePlay.PickCoin.started += PickCoin_started;
                    controls.GamePlay.PickCoin.performed += PickCoin_performed;
                    controls.GamePlay.PickCoin.canceled += PickCoin_canceled;
                    controls.GamePlay.StoreCoin.started += StoreCoin_started;
                }
                
            }
            else
            {
                controls.GamePlay.MoveB.performed += Move_performed;
                controls.GamePlay.MoveB.canceled += Move_canceled;
                controls.GamePlay.JumpB.started += Jump_started;

                if (actionAvailable)
                {
                    controls.GamePlay.PickCoinB.started += PickCoin_started;
                    controls.GamePlay.PickCoinB.performed += PickCoin_performed;
                    controls.GamePlay.PickCoinB.canceled += PickCoin_canceled;
                    controls.GamePlay.StoreCoinB.started += StoreCoin_started;
                }
            }
            controls.Enable();

            groundDetector.GroundedChanged += OnGroundedChanged;
        }

        private void StoreCoin_started(InputAction.CallbackContext obj)
        {
            if (coinThrower.IsThrowing) return;
            coinContainer.ContainCoin();
        }

        private void PickCoin_canceled(InputAction.CallbackContext obj)
        {
            if (coolTimer > 0) return;
            if (coinPicker.HasCoin && coinThrower.IsThrowing)
            {
                coinThrower.Throw();
                coolTimer = coolTime;
            }
        }

        private void PickCoin_performed(InputAction.CallbackContext obj)
        {
            if (coolTimer > 0) return;
            if (coinPicker.HasCoin)
                coinThrower.ThrowPerforming();
        }

        private void Start()
        {
            gravity = rgbody.gravityScale;
            ResetJumpCount();
        }

        private void Update()
        {
            if (coolTimer > 0)
                coolTimer -= Time.deltaTime;
        }

        /// <summary>
        /// 重置x段跳次数
        /// </summary>
        private void ResetJumpCount() => jumpCountLeft = GameVariables.Instance.PlayerJumpCount;

        private void OnGroundedChanged()
        {
            //落地时重置多段跳
            if (groundDetector.IsGrounded)
                ResetJumpCount();
        }

        private void PickCoin_started(InputAction.CallbackContext obj)
        {
            if (!coinPicker.HasCoin)
            {
                if (coinPicker.PickNearbyCoin())
                    coolTimer = coolTime;
            }
        }

        private void Jump_started(InputAction.CallbackContext obj)
        {
            if (jumpCountLeft <= 0) return;
            jumpEffect.Play();
            jumpCountLeft--;
            jumping = true;
            //rgbody.AddForce(Vector2.up * moveSpeed * 10);
        }

        private void Move_canceled(InputAction.CallbackContext obj)
        {
            moveInput = Vector2.zero;
            //UpdateAnim();
        }

        private void Move_performed(InputAction.CallbackContext cxt)
        {
            moveInput = cxt.ReadValue<Vector2>();
            UpdateDir();
            //UpdateAnim();
        }
      

        private void UpdateDir()
        {
            if (Mathf.Abs(moveInput.x) < 0.5f) return;
            if (actionAvailable) coinThrower.PlayerFacing = moveInput.x < 0f ? Direction.Left : Direction.Right;
            spriteRenderer.flipX = moveInput.x > 0f;
        }

        //private void UpdateAnim()
        //{
        //    animator.SetBool("Walking", moveInput != Vector2.zero);
        //}

        private void OnDisable()
        {
            controls.Disable();
            groundDetector.GroundedChanged -= OnGroundedChanged;
        }

        void FixedUpdate()
        {
            Vector2 move = new Vector2();
            move.x = moveInput.x * GameVariables.Instance.PlayerMoveSpeedBase;
            if (jumping)
            {
                move.y = GameVariables.Instance.PlayerJumpForceBase;
                jumping = false;
            }
            else move.y = rgbody.velocity.y;
            //move.y = jumping ? moveSpeed : rgbody.velocity.y;
            rgbody.velocity = move;
            //moveInput = new Vector2(moveInput.x, 0f);
            //if (controls.GamePlay.Jump.WasPressedThisFrame())
            //    rgbody.velocity = new Vector2(moveInput.x * moveSpeed, moveSpeed);
            //else
            //    rgbody.velocity = new Vector2(moveInput.x * moveSpeed, rgbody.velocity.y);

        }

        public void FreezePlayer()
        {
            rgbody.velocity = Vector2.zero;
            rgbody.gravityScale = 0f;
            collider2d.enabled = false;
        }

        public void RestorePlayer()
        {
            rgbody.gravityScale = gravity;
            collider2d.enabled = true;
        }
    }
}