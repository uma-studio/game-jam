using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Hotran.Control.Dir4
{
    public class PlayerController : MonoBehaviour
    {
        private GameControls controls;

        private Rigidbody2D rgbody;
        private Animator animator;

        private Vector2 moveInput;

        [SerializeField]
        private float moveSpeed = 5f;

        private void Awake()
        {
            controls = new GameControls();
            rgbody = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
        }

        private void OnEnable()
        {
            controls.GamePlay.Move.performed += Move_performed;
            controls.GamePlay.Move.canceled += Move_canceled;
            controls.Enable();
        }

        private void Move_canceled(InputAction.CallbackContext obj)
        {
            moveInput = Vector2.zero;
            UpdateAnim();
        }

        private void Move_performed(InputAction.CallbackContext cxt)
        {
            moveInput = cxt.ReadValue<Vector2>();
            UpdateDir();
            UpdateAnim();
        }

        private void UpdateDir()
        {
            animator.SetFloat("DirX", moveInput.x);
            animator.SetFloat("DirY", moveInput.y);
        }

        private void UpdateAnim()
        {           
            animator.SetBool("Walking", moveInput != Vector2.zero);
        }

        private void OnDisable()
        {
            controls.Disable();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            rgbody.velocity = moveInput * moveSpeed;
        }
    }
}