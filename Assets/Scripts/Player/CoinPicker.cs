using Hotran.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPicker : MonoBehaviour
{
    [SerializeField]
    private float radius = 2.5f;

    [SerializeField]
    private LayerMask coinLayer;

    [Tooltip("举起金币的位置")]
    [SerializeField]
    private Transform coinPickPos;
     
    [Tooltip("检测金币的圆心")]
    [SerializeField]
    private Transform coinDetectPos;

    [Tooltip("接到粒子效果")]
    [SerializeField]
    private ParticleSystem pickEffect;

    [SerializeField]
    private AudioClip pickSound;

    //钱币举起时相对玩家的位置
    //private readonly Vector2 pickCoinOffset = Vector2.up * 2;

    //拾取范围内钱币
    private GameObject nearbyCoin;

    private Animator animator;

    private Coin pickedCoin;
    /// <summary>
    /// 当前举起的钱币
    /// </summary>
    public Coin PickedCoin => pickedCoin;//匿名函数在外部调用PickedCoin函数时返回pickedCoin对象

    public bool HasCoin => pickedCoin != null;//匿名函数在外部调用HasCoin函数时返回pickCoin！=null 即不为空

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        var collider = Physics2D.OverlapCircle(coinDetectPos.transform.position, radius, coinLayer);
        if (collider != null)
        {
            if (collider.CompareTag(GameVariables.CoinTag))
                nearbyCoin = collider.gameObject;
        }
        else nearbyCoin = null;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(coinDetectPos.transform.position, radius);
    }

    public bool PickNearbyCoin()
    {
        if (nearbyCoin == null) return false;
        AudioPlayer.Instance.PlaySound(pickSound);
        nearbyCoin.GetComponent<TriggerCollision>().IsColliderGround = false;
        pickedCoin = nearbyCoin.GetComponent<Coin>();
        pickedCoin.transform.SetParent(transform, false);
        pickedCoin.transform.position =coinPickPos.position;
        pickedCoin.transform.localScale=new Vector2(0.5f, 0.5f);//缩
        pickedCoin.TogglePickedState(true);
        pickEffect.Play();
        animator.SetBool("Pick", true);
        return true;
    }

    /// <summary>
    /// 删除手上钱币
    /// </summary>
    public void StoreCoin()
    {
        if (!HasCoin) return;
        Destroy(pickedCoin.gameObject);
        pickedCoin = null;
        animator.SetBool("Pick", false);
    }

    public void LaunchCoin(Vector2 velocity)
    {
        if (pickedCoin == null) return;
        pickedCoin.transform.SetParent(null);
        pickedCoin.TogglePickedState(false);
        pickedCoin.Launch(velocity);
        pickedCoin.transform.localScale = new Vector2(1f, 1f);//放
        pickedCoin = null;
        animator.SetBool("Pick", false);
    }
}
 