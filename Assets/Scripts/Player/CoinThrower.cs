using Hotran.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinThrower : MonoBehaviour
{
    [Tooltip("发射角度")]
    [SerializeField]
    private float theta = 30f;

    //[SerializeField]
    //private float tan_theta = 1;

    [Tooltip("发射指示箭头")]
    [SerializeField]
    private SpriteRenderer launchArrow;

    [SerializeField]
    private AudioClip throwSound;

    float ThrowPressure = 0f;  //蓄力值,也为发射速度
    public float MinThrowPressure = 3f;  //蓄力最小值
    public float MaxThrowPressure = 10f;  // 蓄力最大值

    //private Coin ThrowedCoin;
    private CoinPicker coinPicker;
    [SerializeField]
    private Health mhealth;

    //private Rigidbody2D rgdbody;

    /// <summary>
    /// 角色朝向，朝右为1，朝左为-1
    /// </summary>
    public Direction PlayerFacing { get; set; } = Direction.Right;

    //正在蓄力
    private bool isThrowing = false;
    public bool IsThrowing => isThrowing;
    private Color origincolor;
    //进行初始化
    private void Start()
    {

        //ThrowedCoin = GetComponent<Coin>();
        coinPicker = GetComponent<CoinPicker>();
        //rgdbody = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// 按住投掷蓄力
    /// </summary>
    public void ThrowPerforming()
    {
        if (coinPicker.HasCoin)
        {
            isThrowing = true;
            launchArrow.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// 松开投掷
    /// </summary>
    public void Throw()
    {
        AudioPlayer.Instance.PlaySound(throwSound);
        coinPicker.LaunchCoin(velocity: new Vector2(ThrowPressure * (int)PlayerFacing, ThrowPressure * Mathf.Tan(theta * Mathf.Deg2Rad)));
        ThrowPressure = MinThrowPressure;
        isThrowing = false;
        launchArrow.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (mhealth.IsDead)
        {
            launchArrow.gameObject.SetActive(false);
        }
            if (IsThrowing)
            {

                if (ThrowPressure < MaxThrowPressure)
                {  //如果当前蓄力值小于最大值
                    ThrowPressure += Time.deltaTime * 10f; //则每帧递增当前蓄力值
                launchArrow.GetComponent<Renderer>().material.color = Color.blue;
            }
                else
                {  //达到最大值时，当前蓄力值就等于最大蓄力值
                    ThrowPressure = MaxThrowPressure;
                    launchArrow.GetComponent<Renderer>().material.color = Color.red;
                }
                launchArrow.transform.localScale = new Vector3((int)PlayerFacing * ThrowPressure / 3f, 1f);
                launchArrow.transform.rotation = Quaternion.Euler(0f, 0f, (int)PlayerFacing * theta);
            }
            else ThrowPressure = MinThrowPressure;
            //if (ThrowedCoin.isPicked)  //判断硬币是否被接住
            //{
            //    if (Input.GetButton("throw"))  //hold  按下
            //    {
            //        if (ThrowPressure < MaxThrowPressure)
            //        {  //如果当前蓄力值小于最大值
            //            ThrowPressure += Time.deltaTime * 10f; //则每帧递增当前蓄力值
            //        }
            //        else
            //        {  //达到最大值时，当前蓄力值就等于最大蓄力值
            //            ThrowPressure = MaxThrowPressure;
            //        }
            //        //这时设置动画为蓄力状态动画
            //       // animator.SetFloat("IsPressure", jumpPressure);
            //    }
            //    else 
            //    {
            //        if (ThrowPressure < MinThrowPressure)
            //        {   //如果是轻轻按下就松开则把最小蓄力值赋值给当前蓄力值
            //            //如果是按住不松则把上面递增的值传下来
            //            ThrowPressure = MinThrowPressure;
            //            rgdbody.velocity = new Vector2(0f, ThrowPressure*tan_theta);//y轴速度
            //            rgdbody.velocity = new Vector2(ThrowPressure,0f);//x轴速度
            //            ThrowPressure = 0f; //发射以后把蓄力值重设为0
            //            ThrowedCoin.isPicked = false;  //不被接住设为否
            //        }
            //        //animator.SetFloat("IsPressure", 0f); //设置动画的Float值为0
            //        //animator.SetBool("IsPicked", ThrowedCoin.isPicked); //根据是否在手上播放动画
            //    }
            //}

        }
        //void OnCollisionEnter(Collision other)
        //{
        //    //检测是否碰撞到地面
        //    if (other.gameObject.tag == "Ground")
        //    {
        //        Destroy(this);
        //        //animator
        //    }

        //}
    }

    public enum Direction
    {
        None,
        Right = 1,
        Left = -1
    }


