using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Hotran.Control.Dir2;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public bool IsDead { get; private set; }

    public event UnityAction HealthAtZero;

    public event UnityAction ValueChanged;

    [SerializeField]
    private float maxHealth = 10f;

    
    public float MaxHealth => maxHealth;

    private float _currentHp = -1;

    public float CurrentHealth
    {
        get => _currentHp;
        private set
        {
            if (value <= 0)
            {
                value = 0;
                HealthAtZero?.Invoke();
            }
            _currentHp = value;
            ValueChanged?.Invoke();
        }
    }

    /// <summary>
    /// 无敌时间的设计
    /// </summary>
    public float invicibleTime = 2f;
    public float invincibleTimer = 0f;//计时器
    public bool Isinvincible = false;

    private PlayerController2Dir playerController;
    private SpriteRenderer sp;
    //private Collider2D collider2d;
    private CoinPicker coinPicker;

    [SerializeField]
    private Transform RevivePos;

    [SerializeField]
    private Text ReviveCounterUI;

    private float reviveTimer;

    /// <summary>
    /// 受伤闪烁
    /// </summary>
    private float flashTimer;
    private readonly float flashTime = 1f;
    private readonly string flashAmountName = "_FlashAmount";

    private readonly string timerFormat = "0";

    private void Awake()
    {
        playerController = GetComponent<PlayerController2Dir>();
        sp = GetComponent<SpriteRenderer>();
        //collider2d = GetComponent<Collider2D>();
        coinPicker = GetComponent<CoinPicker>();
    }

    void Start()
    {
        ReviveCounterUI.gameObject.SetActive(false);
        //考虑从存档读取Hp后又初始化
        if (CurrentHealth < 0) CurrentHealth = maxHealth; //需要初始化时才初始化
        HealthAtZero += Die;
    }

    private void Update()
    {
        if(Isinvincible)
        {
            invincibleTimer -= Time.deltaTime;
            if(invincibleTimer<0)
            {
                Isinvincible = false;//倒计时结束后终止无敌状态
            }
        }
        if (flashTimer > 0f)
        {
            flashTimer -= Time.deltaTime;
            sp.material.SetFloat(flashAmountName, flashTimer);
        }
        else if (flashTimer < 0f)
        {
            flashTimer = 0f;
            sp.material.SetFloat(flashAmountName, 0f);
        }

        //复活计时
        if (IsDead)
        {
            if (reviveTimer > 0f)
            {
                reviveTimer -= Time.deltaTime;
                ReviveCounterUI.text = reviveTimer.ToString(timerFormat);
            }
        }
    }

    public void TakeDamage(float damage)
    {//damage小于0为受到伤害
        if (damage < 0)//考虑到回复手段
        {
            if (Isinvincible)
            {
                return;
            }
            Isinvincible = true;
            invincibleTimer = invicibleTime;//重置无敌时间
            //CurrentHealth += damage;
            //animator.SetTrigger("GetHit");
            flashTimer = flashTime;
        }
        CurrentHealth += damage;
        //animator.SetTrigger("GetHeal");
    }

    private void Die()
    {
        if (IsDead) return;
        //animator.SetTrigger("Die");
        IsDead = true;

        //销毁手上硬币
        coinPicker.StoreCoin();

        sp.enabled = false;
        playerController.FreezePlayer();
        playerController.enabled = false;
        //collider2d.enabled = false;

        ReviveCounterUI.gameObject.SetActive(true);
        reviveTimer = GameVariables.Instance.PlayerReviveTime;
        //x秒后复活
        Invoke(nameof(Revive), GameVariables.Instance.PlayerReviveTime);
        //Destroy(gameObject);
    }

    private void Revive()
    {
        CurrentHealth = maxHealth;
        IsDead = false;
        ReviveCounterUI.gameObject.SetActive(false);

        transform.position = RevivePos.position;

        sp.enabled = true;
        playerController.RestorePlayer();
        playerController.enabled = true;
    }
}