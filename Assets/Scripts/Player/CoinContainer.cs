using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinContainer : MonoBehaviour
{
    private int score = 0;
    public int PlayerScore
    {
        get => score;
        set
        {
            score = value;
            CheckUpdate();
        }
    }

    private CoinPicker coinpicker;
    private BackgroundChange bkChange;

    private Health health;
    [Tooltip("对方的血量")]
    [SerializeField]
    private Health targetHealth;

    private int levelIndex = 0;//目前的城市等级
    public int CurrentEraLevel => levelIndex;

    [Tooltip("时代要求分数数据库")]
    [SerializeField]
    private EraData eraData;

    [SerializeField]
    private EraBGM bgM;
    
    private void Awake()
    {
        health = GetComponent<Health>();
        coinpicker = GetComponent<CoinPicker>();
        bkChange = GetComponent<BackgroundChange>();
    }

    private void Start()
    {
        health.HealthAtZero += DeathPunishment;
        targetHealth.HealthAtZero += KillReward;
    }

    private void KillReward()
    {
        //击杀奖励
        //TODO
        PlayerScore *= 2;
    }

    private void DeathPunishment()
    {
        //TODO
        //分数减半
        PlayerScore = (int)(PlayerScore / 2f);
    }

    public void ContainCoin()
    {
        if(coinpicker.HasCoin)
        {
            PlayerScore += coinpicker.PickedCoin.Score;
            coinpicker.StoreCoin();
            //CheckUpdate();
            //Destroy(coinpicker.PickedCoin.gameObject);//摧毁硬币
            //animator()摧毁动画演示
        }
        
    }

    private void CheckUpdate()
    {
        int eraIndex = 0;
        for (int i = 0; i < eraData.LevelsScores.Length; i++)
        {
            if (PlayerScore >= eraData.LevelsScores[i])
                eraIndex = i;
        }
        if (eraIndex != levelIndex)
        {
            levelIndex = eraIndex;
            UpdateEra();
        }
        //foreach (var eraScore in eraData.LevelsScores)
        //{
        //    if (PlayerScore >= eraScore)
        //        eraIndex = eraScore;
        //}
        //if (levelIndex <= eraData.LevelsScores.Length - 1 && levelIndex >= 0)
        //{ 
        //    if (PlayerScore >= eraData.LevelsScores[levelIndex + 1])
        //    {
        //        UpdateEra();
        //    }
        //}
    }

    private void UpdateEra()
    {
        bkChange.ChangeEra(levelIndex);
        bgM.ChangeEraBGM(levelIndex);
    }
}
