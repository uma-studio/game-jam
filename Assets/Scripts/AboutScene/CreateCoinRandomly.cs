using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateCoinRandomly : MonoBehaviour
{ 
    public float mincoinX = -6f;
    public float maxcoinX = -4f;
    public float mincoinY = 5f;
    public float maxcoinY = 7f;//生成位置
    public float minintervalTime = 0.1f;
    public float maxintervalTime = 2f;
   // public float intervalTime = 2f;//初始间隔时间
    //public float minintervalTime = 0.2f;
    //public float maxintervalTime = 4f;
    public float intervalTime=2f;//固定间隔时间
    //private GameObject[] UseCoin;

    [SerializeField]
    private Coin m_coin;
    public Coin CoinPrefab
    {
        get => m_coin;
        set => m_coin = value;
    }

    //public GameObject Coin1;
    //public GameObject Coin2;

    [SerializeField]
    private CoinContainer playerCoins;

    public float focerang = 150f;
    // Start is called before the first frame update
    //private void Awake()
    //{
    //    m_coin = Coin1;


    //}
    void Start()
    {
        // Debug.Log("start成功");
       // m_coin.GetComponent<TriggerCollision>().enabled = false;
        // Debug.Log("start成功");
        InvokeRepeating(nameof(Createcoin), 4F, intervalTime);
    }
    //private void Update()
    //{if (playerCoins.PlayerScore >= 5)
    //    {
    //        m_coin = Coin2;
    //    }
    //}
    void Createcoin()
    {
        intervalTime = Random.Range(minintervalTime, maxintervalTime);
       // Vector2 location = new Vector2(Random.Range(mincoinX, maxcoinX);
        
        //intervalTime = Random.Range(minintervalTime, maxintervalTime);
        Vector2 location = new Vector2(Random.Range(mincoinX, maxcoinX),
                                        Random.Range(mincoinY, maxcoinY));
       Coin coinCopied = Instantiate(m_coin, location, Quaternion.identity);
        //需复制一个对象，所以不能把对象全部destory（至少留有一个）
        coinCopied.GetComponent<TriggerCollision>().enabled = true;
        Rigidbody2D RGcoinCopied = coinCopied.GetComponent<Rigidbody2D>();
        //模拟抛物      
        RGcoinCopied.AddForce
            (new Vector2
                (
                     Random.Range
                        (
                                Random.Range(-focerang, -100f), Random.Range(100f, focerang)
                        )
                    , 10f
                )
            );//（-200，-50）U(50,200)//Debug.Log("重复创建成功");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var p1 = new Vector2(mincoinX, mincoinY);
        var p2 = new Vector2(mincoinX, maxcoinY);
        var p3 = new Vector2(maxcoinX, maxcoinY);
        var p4 = new Vector2(maxcoinX, mincoinY);
        //生成范围
        Gizmos.DrawLine(p1, p2);
        Gizmos.DrawLine(p2, p3);
        Gizmos.DrawLine(p3, p4);
        Gizmos.DrawLine(p4, p1);

    }
}