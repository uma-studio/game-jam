using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCollision : MonoBehaviour
{
    private int ColliserTime;
    public int MaxColliserTime=200;
    public AudioClip Stone;
    public AudioSource Source;
    private bool isColliderGround = false;

    [SerializeField]
    public Coin Abstacle;
    public bool IsColliderGround
    {
        get => isColliderGround;
        set => isColliderGround = value;
    }

    [Tooltip("爆炸粒子特效")]
    [SerializeField]
    private ParticleSystem impactEffectPrefab;
    private void Awake()
    {//声音
        //Stone = this.GetComponent<AudioClip>();
       
        Source = this.GetComponent<AudioSource>();

        Abstacle = this.gameObject.GetComponent<Coin>();
    }
    //确保如果碰到地面但及时收钱，钱币就不会消失
    void OnCollisionEnter2D(Collision2D other)
    {
       // Debug.Log("碰撞了");
        if (other.gameObject.CompareTag(GameVariables.GroundTag))
        { Source.PlayOneShot(Stone, 1F);
            
            isColliderGround = true;
        }
            else
            isColliderGround = false;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GroundColliderForCoin"))
        {//Debug.Log("空气墙");
            if (Abstacle.isPicked==false)
            {
                //Debug.Log("未拾起");
                DestroyCoin();

            }

        }
    }
    private void Update()
    {
        if (isColliderGround)
        {
            ColliserTime++;
            
        }
            if (ColliserTime>= MaxColliserTime)      
                DestroyCoin();
    }

    private void DestroyCoin()
    {
        //生成粒子特效
        var effect = Instantiate(impactEffectPrefab, transform.position, Quaternion.identity);
        //一秒后删除特效
        Destroy(effect.gameObject, 1f);
        Destroy(this.gameObject);
    }
}
