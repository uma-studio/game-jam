using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// 代表一个时代的所有表示物
/// </summary>
public class Era : MonoBehaviour
{
    public RuntimeAnimatorController playerAnimator;

    [SerializeField]
    private SpriteRenderer[] images;

    [SerializeField]
    private Tilemap tilemap;

    [SerializeField]
    private Coin coinPrefab;
    public Coin CoinPrefab => coinPrefab;

    /// <summary>
    /// 切换激活/隐藏
    /// </summary>
    public void Toggle(bool active)
    {
        foreach (var image in images)
        {
            image.gameObject.SetActive(active);
        }
        tilemap.gameObject.SetActive(active);
    }
}
