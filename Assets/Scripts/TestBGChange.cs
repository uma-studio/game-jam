using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBGChange : MonoBehaviour
{
    public Animator ani;     

    private void Update()
    {
      if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            SetCarIdle();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {

            SetCarExplosive();

        }

    }

    /// <summary>

    /// 设置 车 的状态为 Idle

    /// </summary>

    private void SetCarIdle()

    {

        ani.SetInteger("Animation_Int", 0);

    }

    /// <summary>

    /// 设置 车 的状态为 Explosive

    /// </summary>

    private void SetCarExplosive()
    {

        ani.SetInteger("Animation_Int", 1);

    }

}
