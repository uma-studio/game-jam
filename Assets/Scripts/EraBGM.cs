using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EraBGM : MonoBehaviour
{
    private AudioSource audioSource;

    private int highestLevel = 0;

    [SerializeField]
    private AudioClip[] eraBGMs;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = eraBGMs[0];
    }

    public void ChangeEraBGM(int level)
    {
        if (level > highestLevel)
        {
            highestLevel = level;
            audioSource.clip = eraBGMs[level];
            audioSource.Play();
        }
    }
}
