using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Hotran.Control.Dir2.PlayerController2Dir;

/// <summary>
/// 游戏用到的变量
/// </summary>
public class GameVariables : MonoBehaviour
{
    public static GameVariables Instance { get; private set; }

    public static Ending GameEnding { get; set; } = Ending.Null;
    public static PlayerTag Winner { get; set; }

    // 无需调整的常量为Static
    public static readonly string CoinTag = "Coin";
    public static readonly string PlayerTag = "Player";
    public static readonly string GroundTag = "Ground";

    public int Test = 3;

    //以下为要按需调整的变量...
    /// <summary>
    /// 玩家基础移动速度
    /// </summary>
    public float PlayerMoveSpeedBase = 5f;

    /// <summary>
    /// 玩家基础跳跃力
    /// </summary>
    public float PlayerJumpForceBase = 10f;

    /// <summary>
    /// 玩家可以x段跳
    /// </summary>
    public int PlayerJumpCount = 2;

    /// <summary>
    /// 玩家的无敌时间
    /// </summary>
    public float invincibleTime = 2f;

    /// <summary>
    /// 玩家复活时间
    /// </summary>
    public float PlayerReviveTime = 3f;

    /// <summary>
    /// 游戏时间-秒
    /// </summary>
    public float GameTime = 300f;
    

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }
}
