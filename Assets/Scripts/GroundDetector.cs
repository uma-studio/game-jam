using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 为角色检测地面
/// </summary>
public class GroundDetector : MonoBehaviour
{
    [Tooltip("检测脚下地面的距离")]
    [SerializeField]
    private float checkGroundRadius = 0.1f;

    [SerializeField]
    private LayerMask groundLayer;

    [SerializeField]
    private LayerMask playerLayer;

    private bool isGrounded;

    public bool IsGrounded
    {
        get => isGrounded;
        set 
        { 
            if (isGrounded != value)
            {
                isGrounded = value;
                //落地/起跳时触发事件
                GroundedChanged?.Invoke();
            }
            isGrounded = value; 
        }
    }

    //落地/起跳时触发
    public event Action GroundedChanged;

    // Update is called once per frame
    void Update()
    {
        //var collider = Physics2D.OverlapCircle(transform.position, checkGroundRadius, groundLayer | playerLayer);
        IsGrounded = CheckGrounded();
    }

    private bool CheckGrounded()
    {
        var collider = Physics2D.OverlapCircle(transform.position, checkGroundRadius, groundLayer);
        if (collider != null) return true;
        var colliders = Physics2D.OverlapCircleAll(transform.position, checkGroundRadius, playerLayer);
        foreach (var colli in colliders)
        {
            if (colli.transform != transform.parent) return true;
        }
        return false;
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag(GameVariables.GroundTag) ||
    //        collision.gameObject.CompareTag(GameVariables.PlayerTag))
    //        IsGrounded = true;
    //}

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    IsGrounded = false;
    //}
}
