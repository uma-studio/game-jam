using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BackgroundChange : MonoBehaviour
{
    private Animator animator;

    [SerializeField]
    private CreateCoinRandomly coinCreator;

    [SerializeField]
    private Animator transitionAnim;

    [SerializeField]
    private Era[] eraMaps;

    private Era currentEra;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        foreach (var era in eraMaps)
        {
            era.Toggle(false);
        }
        //加载初始时代
        ChangeEraImmediate(0);
    }

    /// <summary>
    /// 切换时代
    /// </summary>
    /// <param name="index">时代编号</param>
    public void ChangeEra(int index)
    {
        StartCoroutine(DoChangeEra(index));
    }

    public void ChangeEraImmediate(int index)
    {
        SwitchEra(index);
    }

    private IEnumerator DoChangeEra(int index)
    {
        transitionAnim.SetTrigger("Transit");

        yield return new WaitForSeconds(1f);

        //Load Scene
        SwitchEra(index);
    }

    private void SwitchEra(int index)
    {
        Era era = currentEra;
        currentEra = eraMaps[index];
        currentEra.Toggle(true);
        animator.runtimeAnimatorController = currentEra.playerAnimator;
        coinCreator.CoinPrefab = currentEra.CoinPrefab;
        era?.Toggle(false);
    }
}
