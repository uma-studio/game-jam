using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Hotran.Control.Dir2;

public class WinChecker : MonoBehaviour
{
    [SerializeField]
    private PlayerController2Dir playerA;
    [SerializeField]
    private CoinContainer playerACoins;

    [SerializeField]
    private PlayerController2Dir playerB;
    [SerializeField]
    private CoinContainer playerBCoins;

    private Text timerText;

    private float gameTimer;

    private void Awake()
    {
        timerText = GetComponent<Text>();
    }

    // Start is called before the first frame update
    void Start()
    {
        gameTimer = GameVariables.Instance.GameTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameTimer > 0f)
        {
            gameTimer -= Time.deltaTime;
            timerText.text = gameTimer.ToString("0");
            if (gameTimer <= 0f)
                Win();
        }
    }

    private void Win()
    {
        int futureSum = 0;
        if (playerACoins.CurrentEraLevel == 3) futureSum++;
        if (playerBCoins.CurrentEraLevel == 3) futureSum++;
        //var levelSum = playerA.CurrentEraLevel + playerB.CurrentEraLevel;
        Ending ending = Ending.Null;
        PlayerController2Dir winner = null;
        
        if (futureSum == 2)
            ending = Ending.One;
        else if (futureSum == 1)
        {
            winner = playerACoins.CurrentEraLevel == 3 ? playerA : playerB;
            ending = Ending.Two;
        }
        else if (futureSum == 0)
        {
            //˫Զ��
            if (playerACoins.CurrentEraLevel + playerBCoins.CurrentEraLevel == 0)
                ending = Ending.Four;
            else
            {
                winner = playerACoins.CurrentEraLevel > playerBCoins.CurrentEraLevel ? playerA : playerB;
                ending = Ending.Three;
            }
        }

        GameVariables.GameEnding = ending;

        if (winner != null)
        {
            GameVariables.Winner = winner.Tag;
        }

        SceneManager.LoadScene(2);
    }
}

public enum Ending
{
    Null,
    One,
    Two,
    Three,
    Four
}


