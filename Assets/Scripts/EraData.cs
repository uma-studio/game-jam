using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EraData")]
public class EraData : ScriptableObject
{
    /// <summary>
    /// 达到某个时代需要的分数
    /// </summary>
    public int[] LevelsScores;
}
