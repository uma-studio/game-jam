using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hotran.UI
{
    //血条UI
    public class HealthUI : MonoBehaviour
    {
        [SerializeField]
        private Health bind;

        private Image image;
        // Start is called before the first frame update
        void Awake()
        {
            image = GetComponent<Image>();
        }

        private void OnEnable()
        {
            bind.ValueChanged += UpdateValue;
        }

        private void OnDisable()
        {
            bind.ValueChanged -= UpdateValue;
        }

        private void UpdateValue()
        {
            image.fillAmount = bind.CurrentHealth / bind.MaxHealth;
        }
    }
}