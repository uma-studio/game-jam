using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{
    [SerializeField]
    private Button startButton;

    [SerializeField]
    private Button quitButton;

    [SerializeField]
    private int startSceneIndex = 1;

    [SerializeField]
    private SceneTransition sceneTransition;

    private AsyncOperation loadSceneOperation;

    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(StartGame);
        quitButton.onClick.AddListener(() => Application.Quit());

        loadSceneOperation = SceneManager.LoadSceneAsync(startSceneIndex);
        loadSceneOperation.allowSceneActivation = false;
    }

    private void StartGame()
    {
        sceneTransition.LoadScene(loadSceneOperation);
    }
}
