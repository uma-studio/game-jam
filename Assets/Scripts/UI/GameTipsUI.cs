using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTipsUI : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup tips;

    [SerializeField]
    private float hideTime = 5f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(HideTips), hideTime);
    }

    private void HideTips() => StartCoroutine(DoHide());

    private IEnumerator DoHide()
    {
        while (tips.alpha > 0)
        {
            tips.alpha -= Time.deltaTime;
            yield return null;
        }
        tips.gameObject.SetActive(false);
    }


}
