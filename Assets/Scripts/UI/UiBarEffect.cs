using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hotran.UI
{
    //血条缓动效果
    public class UiBarEffect : MonoBehaviour
    {
        private Image image;

        //要应用缓动效果的血条图片
        [SerializeField]
        private Image target;

        //缓动速度
        [SerializeField]
        private float speed = 3f;

        // Start is called before the first frame update
        void Awake()
        {
            image = GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {
            if (image.fillAmount > target.fillAmount)
            {
                image.fillAmount -= speed * Time.deltaTime;
            }
            else image.fillAmount = target.fillAmount;
        }
    }
}