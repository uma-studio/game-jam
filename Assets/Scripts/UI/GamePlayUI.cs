using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayUI : MonoBehaviour
{
    [SerializeField]
    private Button quitButton;

    [SerializeField]
    private Button muteButton;

    [SerializeField]
    private AudioSource musicSource;

    private bool muted = false;

    // Start is called before the first frame update
    void Start()
    {
        quitButton.onClick.AddListener(() =>
            SceneManager.LoadScene(0)
        );
        muteButton.onClick.AddListener(Mute);
    }

    private void Mute()
    {
        muted = !muted;
        musicSource.volume = muted ? 0f : 1f;
    }
}
