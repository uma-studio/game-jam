using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndingPresenter : MonoBehaviour
{
    [SerializeField]
    private Text text;

    [TextArea(3, 10)]
    [SerializeField]
    private string[] endingTexts;

    // Start is called before the first frame update
    void Start()
    {
        text.text = endingTexts[(int)GameVariables.GameEnding];
    }
}
