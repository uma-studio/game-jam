using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    private Text text;

    private int score;

    [SerializeField]
    private CoinContainer bindPlayer;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        //TODO
        if (score != bindPlayer.PlayerScore)
        {
            score = bindPlayer.PlayerScore;
            text.text = score.ToString();
        }
    }

}
