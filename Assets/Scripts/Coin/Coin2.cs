using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin2 : MonoBehaviour
{
    /// <summary>
    /// 此币的分数
    /// </summary>
    public int Score = 2;

    private Rigidbody2D rgbody;
    private Collider2D collider2d;

    /// <summary>
    /// 判断硬币是否被捡起的变量
    /// </summary>
    public bool isPicked = false;

    private void Awake()
    {
        rgbody = GetComponent<Rigidbody2D>();
        collider2d = GetComponent<Collider2D>();
    }

    /// <summary>
    /// 切换被举起的状态
    /// </summary>
    /// <param name="picked"></param>
    public void TogglePickedState(bool picked)
    {
        if (picked)
        {
            isPicked = picked;
            Destroy(rgbody);
        }
        else rgbody = gameObject.AddComponent<Rigidbody2D>();
        //rgbody.gravityScale = picked ? 0f : 1f;
        collider2d.enabled = picked ? false : true;
    }

    public void Launch(Vector2 velocity)
    {
        //TogglePickedState(false);
        rgbody.velocity = velocity;
    }

    //落地时消失
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if (collision.gameObject.CompareTag(GameVariables.GroundTag))
        //    Destroy(gameObject);
        if (collision.gameObject.CompareTag(GameVariables.PlayerTag))
        {
            //TODO: 玩家被击中受伤害=？
            collision.gameObject.GetComponent<Health>().TakeDamage(-Score);
            Destroy(gameObject);
        }
    }
}

