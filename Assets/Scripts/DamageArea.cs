using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hotran.UI;
public class DamageArea : MonoBehaviour
{
    /// <summary>
    /// 伤害陷阱
    /// </summary>
    [SerializeField]
    private float Injury = 1f;//陷阱造成的伤害量


    //可以落到陷阱中，但不脱离会持续掉血
    public void OnTriggerStay2D(Collider2D collision)
    {
        Health PlayerHealth = collision.GetComponent<Health>();//玩家要装备Health组件
        HealthUI PlayerHealthUI = collision.GetComponent<HealthUI>();


        if (PlayerHealth != null)
        {
            PlayerHealth.TakeDamage(Injury);//玩家扣血
            //血条进行操作
            //PlayerHealthUI
        }
    }

    //接触但并不落入陷阱中，不移开会持续掉血
    public void OnCollisionEnter2D(Collision2D collision)
    {
        Health PlayerHealth = collision.gameObject.GetComponent<Health>();//玩家要装备Health组件
        HealthUI PlayerHealthUI = collision.gameObject.GetComponent<HealthUI>();


        if (PlayerHealth != null)
        {
            PlayerHealth.TakeDamage(Injury);//玩家扣血
            //血条进行操作
            //PlayerHealthUI
        }
    }


}
